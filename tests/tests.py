import logging
import random
import string
import sys
import unittest
import os
from pathlib import Path
from S3Tools.main import S3Tools
from app import prepare_folder_path

logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s [%(levelname)s] %(message)s',
    handlers=[
        logging.StreamHandler(sys.stdout)
    ]
)


class TestDownload(unittest.TestCase):
    TEST_FILE_KEY = '{}/TEST/test.txt'.format("".join([random.choice(string.ascii_letters) for _ in range(10)]))
    TEST_URL = 'https://seznam.cz'
    TEST_REQUEST_METHOD = 'POST'

    def setUp(self):
        if all([os.getenv("S3_ENDPOINT_URL"), os.getenv("S3_ACCESS_KEY"), os.getenv('S3_SECRET_KEY'),
                os.getenv('TEST_BUCKET')]):
            self.s3 = S3Tools(os.getenv("S3_ENDPOINT_URL"),
                              os.getenv("S3_ACCESS_KEY"),
                              os.getenv("S3_SECRET_KEY"))
            self.TEST_BUCKET = os.getenv('TEST_BUCKET')
        else:
            logging.error('ENV VARIABLES not defined.')

    def test_s3_download_and_zip(self):
        # Download test file from sample API using `s3Tools.download_file_to_s3`
        logging.info(f'Attempting to download file from {self.TEST_URL} to {self.TEST_FILE_KEY} ... ')
        self.assertTrue(
            self.s3.download_file_to_s3(self.TEST_BUCKET, self.TEST_URL, self.TEST_FILE_KEY, self.TEST_REQUEST_METHOD)
        )

        # Confirm existence
        self.assertTrue(self.s3._s3_file_exists(self.TEST_BUCKET, self.TEST_FILE_KEY))

        # Try zipping the parent folder
        logging.info('Zipping parent directory of test file ...')
        folder_path = '/'.join(self.TEST_FILE_KEY.split('/')[:-1])
        self.s3.zip_folder(self.TEST_BUCKET, folder_path, temp_dir_path=Path('.'))
        logging.info('Zipping succesful.')

        # Confirm zipped file
        self.assertTrue(self.s3._s3_file_exists(self.TEST_BUCKET, folder_path + '.zip'))
        self.assertFalse(self.s3._s3_file_exists(self.TEST_BUCKET, self.TEST_FILE_KEY))

        # Delete zipped test file
        logging.info('Deleting zip file.')
        self.s3.s3.meta.client.delete_object(Key=folder_path + '.zip', Bucket=self.TEST_BUCKET)

        # Confirm deletion
        self.assertFalse(self.s3._s3_file_exists(self.TEST_BUCKET, folder_path + '.zip'))

    def test_invalid_folder_path(self):
        self.assertRaises(ValueError, prepare_folder_path, "", False)
        self.assertRaises(ValueError, prepare_folder_path, "/", False)
        self.assertRaises(ValueError, prepare_folder_path, "///", False)

    def tearDown(self):
        del self.s3
