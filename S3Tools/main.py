import json
import logging
import os
import random
import string
import tempfile
import zipfile
from pathlib import Path
import boto3
import requests
from botocore.exceptions import ClientError


class S3Tools:
    def __init__(self, endpoint_url, access_key, secret_key):
        self.s3 = boto3.resource(
            's3',
            endpoint_url=endpoint_url,
            aws_access_key_id=access_key,
            aws_secret_access_key=secret_key
        )

    def download_file_to_s3(self, bucket, url, file_name, request_method, request_header={}, request_body='',
                            request_other={}):
        try:
            request_body = json.dumps(request_body) if request_body is not None else None
            r = requests.request(method=request_method, url=url, headers=request_header, data=request_body,
                                 **request_other)
            if r.status_code == 200:
                logging.info(f"Downloading file: {url} to bucket: {bucket}/{file_name}")
                try:
                    self.s3.meta.client.put_object(Key=file_name, Body=r.content, Bucket=bucket)
                except ClientError as e:
                    logging.error(e)
                    return False
                return True
            logging.error(f'Bad request {url}. Reason: {r.reason}')
            return False
        except Exception as e:
            logging.error(e)
            return False

    def zip_folder(self, bucket, folder_path, temp_dir_path):
        temp_dir_path.mkdir(parents=True, exist_ok=True)

        with tempfile.TemporaryDirectory(dir=temp_dir_path) as tmp_dir:
            s3bucket = self.s3.Bucket(bucket)
            zip_filename = "s3_" + "".join([random.choice(string.ascii_letters) for _ in range(10)]) + ".zip"
            zip_filepath = folder_path + '.zip'
            self._check_that_file_does_not_exists(bucket, zip_filepath)
            local_zip_path = Path(f'{tmp_dir}/{zip_filename}')
            try:
                with zipfile.ZipFile(local_zip_path, 'w', zipfile.ZIP_DEFLATED) as zipf:
                    files = []
                    fcount = 0
                    logging.info(f"Processing objects in {folder_path}/ ...")
                    for obj in s3bucket.objects.filter(Prefix=folder_path + '/'):
                        files.append(obj.key)
                        self._add_s3_file_to_zip(obj, folder_path, tmp_dir, s3bucket, zipf)
                        fcount = fcount + 1
                        if (fcount % 1000) == 0:
                            logging.info(f"{fcount} objects processed.")
                # upload zip
                self._check_that_file_does_not_exists(bucket, zip_filepath)
                self.s3.meta.client.upload_file(str(local_zip_path), bucket, zip_filepath)
            finally:
                os.unlink(local_zip_path)
            logging.info(f"Object {folder_path} was uploaded.")
            # delete S3 files
            for file in files:
                self.s3.Object(bucket, file).delete()
                logging.info(f"Object {file} was deleted.")

    def _s3_file_exists(self, bucket, path):
        try:
            self.s3.Object(bucket, path).load()
            return True
        except ClientError as ex:
            if ex.response['Error']['Code'] == "404":
                return False
            raise ex

    def _check_that_file_does_not_exists(self, bucket, path):
        if self._s3_file_exists(bucket, path):
            raise ValueError(f'File {path} already exists!')

    @staticmethod
    def _add_s3_file_to_zip(obj, folder_path, tmp_dir, s3bucket, zipf):
        object_name = obj.key[len(folder_path + '/'):]
        if len(object_name) == 0 or object_name.endswith('/'):
            return
        local_path = os.path.join(tmp_dir, object_name)
        dir_path = os.path.dirname(local_path)
        if not os.path.exists(dir_path):
            os.mkdir(dir_path)
        s3bucket.download_file(obj.key, local_path)
        zipf.write(local_path, object_name)
        os.unlink(local_path)
