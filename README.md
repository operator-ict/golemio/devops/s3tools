# Description
S3 tools helps manage files on s3.

# Running the process
Go to the project folder in terminal and run `python app.py`.

# Start docker container
```bash
docker run -ti --rm -e S3_ENDPOINT_URL="https://XXXXX" -e S3_ACCESS_KEY="XXXXX" -e S3_SECRET_KEY="XXXXX"  registry.gitlab.com/operator-ict/golemio/devops/s3tools/master:latest bash
```



