FROM python:3.9-slim-bullseye

WORKDIR /usr/src/app

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
    vim \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

COPY --chown=root:root ./cronpy /etc/cron.d/cronpy
RUN chmod 644 /etc/cron.d/cronpy

COPY requirements.txt app.py run-as-cron.sh ./
RUN pip install -r requirements.txt && touch /var/log/task.log

COPY . .

CMD ["cron", "&&", "tail", "-f", "/var/log/task.log"]
