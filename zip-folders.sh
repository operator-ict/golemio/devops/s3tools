#!/bin/bash

if [ "$#" -ne 6 ]; then
    echo "ERROR Wrong arguments - use command like: nohup $0 BUCKET FOLDER_PATH YEAR MONTH START_DAY END_DAY &"
    exit 1
fi

for i in $(seq -f "%02g" $5 $6)
do
    python -u /usr/src/app/app.py --task zip --bucket $1 --key $2/$3-$(printf "%02d" $4)-"$i"
done
